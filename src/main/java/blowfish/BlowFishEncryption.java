package blowfish;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.security.Key;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class BlowFishEncryption {
	
	private static final String ALGORITHM = "Blowfish";
	private static String keyString = "DesireSecretKey";

	public static void encrypt(File inputFile, File outputFile)
			throws Exception {
		doCrypto(Cipher.ENCRYPT_MODE, inputFile, outputFile);
		System.out.println("File encrypted successfully!");
	}

	public static void decrypt(File inputFile, File outputFile)
			throws Exception {
		doCrypto(Cipher.DECRYPT_MODE, inputFile, outputFile);
		System.out.println("File decrypted successfully!");
	}

	private static void doCrypto(int cipherMode, File inputFile,
			File outputFile) throws Exception {

		Key secretKey = new SecretKeySpec(keyString.getBytes(), ALGORITHM);
		Cipher cipher = Cipher.getInstance(ALGORITHM);
		cipher.init(cipherMode, secretKey);

		FileInputStream inputStream = new FileInputStream(inputFile);
		byte[] inputBytes = new byte[(int) inputFile.length()];
		inputStream.read(inputBytes);

		byte[] outputBytes = cipher.doFinal(inputBytes);

		FileOutputStream outputStream = new FileOutputStream(outputFile);
		outputStream.write(outputBytes);

		inputStream.close();
		outputStream.close();

	}

	public static void main(String[] args) throws Exception {
    	long start_time = System.nanoTime();

		File inputFile = new File("file.txt"); //plain_image.raw
		File encryptedFile = new File("encrypted2.raw");
		
		File decryptedFile = new File("decrypted2.raw");

		try {
			BlowFishEncryption.encrypt(inputFile, encryptedFile);
			BlowFishEncryption.decrypt(encryptedFile, decryptedFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		long end_time = System.nanoTime();
		double difference = (end_time - start_time)/1e6;
        System.out.println("TIME:" + difference + " ms");

	}
	

}