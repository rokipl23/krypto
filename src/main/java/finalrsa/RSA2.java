package finalrsa;

import org.apache.commons.codec.binary.Hex;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.util.Vector;

/**
 * Created by tomaszkowalski on 02.06.2017.
 */
public class RSA2 {

    private Cipher cipher;
    private KeyPairGenerator kpg;
    private KeyPair keypair;

    public static void main(String[] args){
        RSA2 rsa = new RSA2();
        try {
            rsa.initVariables();
            rsa.encryptFile("test.txt");
            rsa.decryptFile("testENCR.txt");

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
        catch (Exception e){
            e.printStackTrace();
        }


    }

    private void initVariables() throws NoSuchAlgorithmException, NoSuchPaddingException{
        kpg = KeyPairGenerator.getInstance("RSA") ;
        kpg.initialize(1024);
        this.keypair = kpg.generateKeyPair();
        this.cipher = Cipher.getInstance("RSA");

    }
    private byte[] append(byte[] prefix, byte[] suffix){
        byte[] toReturn = new byte[prefix.length + suffix.length];
        for (int i=0; i< prefix.length; i++){
            toReturn[i] = prefix[i];
        }
        for (int i=0; i< suffix.length; i++){
            toReturn[i+prefix.length] = suffix[i];
        }
        return toReturn;
    }

    private byte[] blockCipher(byte[] bytes, int mode) throws IllegalBlockSizeException, BadPaddingException {
        // string initialize 2 buffers.
        // scrambled will hold intermediate results
        byte[] scrambled = new byte[0];

        // toReturn will hold the total result
        byte[] toReturn = new byte[0];
        // if we encrypt we use 100 byte long blocks. Decryption requires 128 byte long blocks (because of RSA)
        int length = (mode == Cipher.ENCRYPT_MODE)? 100 : 128;

        // another buffer. this one will hold the bytes that have to be modified in this step
        byte[] buffer = new byte[length];

        for (int i=0; i< bytes.length; i++){

            // if we filled our buffer array we have our block ready for de- or encryption
            if ((i > 0) && (i % length == 0)){
                //execute the operation
                scrambled = cipher.doFinal(buffer);
                // add the result to our total result.
                toReturn = append(toReturn,scrambled);
                // here we calculate the length of the next buffer required
                int newlength = length;

                // if newlength would be longer than remaining bytes in the bytes array we shorten it.
                if (i + length > bytes.length) {
                    newlength = bytes.length - i;
                }
                // clean the buffer array
                buffer = new byte[newlength];
            }
            // copy byte into our buffer.
            buffer[i%length] = bytes[i];
        }

        // this step is needed if we had a trailing buffer. should only happen when encrypting.
        // example: we encrypt 110 bytes. 100 bytes per run means we "forgot" the last 10 bytes. they are in the buffer array
        scrambled = cipher.doFinal(buffer);

        // final step before we can return the modified data.
        toReturn = append(toReturn,scrambled);

        return toReturn;
    }

    public String decrypt(String encrypted) throws Exception{
        this.cipher.init(Cipher.DECRYPT_MODE, this.keypair.getPrivate());
        byte[] bts = Hex.decodeHex(encrypted.toCharArray());

        byte[] decrypted = blockCipher(bts, Cipher.DECRYPT_MODE);

        return new String(decrypted,"UTF-8");
    }

    public String encrypt(String plaintext) throws Exception{
        this.cipher.init(Cipher.ENCRYPT_MODE, this.keypair.getPublic());
        byte[] bytes = plaintext.getBytes("UTF-8");

        byte[] encrypted = blockCipher(bytes,Cipher.ENCRYPT_MODE);

        char[] encryptedTranspherable = Hex.encodeHex(encrypted);
        return new String(encryptedTranspherable);
    }

    private String getFileNameWithoutExtension(String fileName)
            throws Exception {
        String toReturn = null;
        if (fileName != null) {
            int indexOfDot = fileName.indexOf(".");
            toReturn = fileName.substring(0, indexOfDot);
        }
        System.out.println("fileNameWithoutExtension => " + toReturn);
        return toReturn;
    }
    public File encryptFile(String fileToEncrypt) throws Exception {
        File toReturn = null;
        InputStream in = new FileInputStream(fileToEncrypt);
        //URL url = this.getClass().getResource(fileToEncrypt);
        //String path = url.getPath();
       // path = path.substring(0, path.lastIndexOf("/"));
        //System.out.println("path => " + path);
        String fileAsString = new String(this.readInputStream(in), "UTF-8");
        String encryptedText = this.encrypt(fileAsString);
        toReturn = new File("/Users/tomaszkowalski/IdeaProjects/krypto/"
        + getFileNameWithoutExtension(fileToEncrypt) + "ENCR.txt");
        if (toReturn.exists()) {
            toReturn.delete();
        }
        toReturn.createNewFile();
        FileWriter fw = new FileWriter(toReturn);
        fw.write(encryptedText);
        fw.flush();
        fw.close();
        System.out.println("FINISHED ENCRYPTING");
        return toReturn;
    }
    public File decryptFile(String fileToDecrypt) throws Exception {
        File toReturn = null;
        InputStream in =  new FileInputStream(fileToDecrypt);//this.getClass().getResourceAsStream(fileToDecrypt);
        //URL url = this.getClass().getResource(fileToDecrypt);
        //String path = url.getPath();
        //path = path.substring(0, path.lastIndexOf("/"));
        //System.out.println("path => " + path);
        String fileAsString = new String(this.readInputStream(in), "UTF-8");
        String decryptedText = this.decrypt(fileAsString);
        toReturn = new File("/Users/tomaszkowalski/IdeaProjects/krypto/"
        + getFileNameWithoutExtension(fileToDecrypt)+"DECR.txt");
        if (toReturn.exists()) {
            toReturn.delete();
        }
        toReturn.createNewFile();
        FileWriter fw = new FileWriter(toReturn);
        fw.write(decryptedText);
        fw.flush();
        fw.close();
        System.out.println("FINISHED DECRYPTING");
        return toReturn;
    }

    private byte[] readInputStream(InputStream in) throws Exception {
        try {
            // System.out.println("in => "+in);
            byte[] toReturn = new byte[0];
            Vector intermediate = new Vector();
            if (in != null) {
                int b = -1;
                while ((b = in.read()) != -1) {
                    intermediate.add((byte) b);
                }
            // intermediate.add((byte)b);
            }
            // System.out.println("intermediate => "+intermediate);
            // System.out.println("intermediate.size() => "+intermediate.size());
            toReturn = new byte[intermediate.size()];
            for (int i = 0; i < intermediate.size(); i++) {
                toReturn[i] = (Byte) intermediate.elementAt(i);
            }
            return toReturn;
        } finally {
            if (in != null)
                in.close();
        }
    }
}
